module.exports = function(grunt){
  grunt.registerMultiTask('outline', 'grunt plugin to outline inlined block to external files', function() {

        path = require('path'),
        options = this.options();

    

    this.files.forEach(function(file) {
      file.src.forEach(function(src) {
        var dest;
        var deps = {};

        if (!grunt.file.exists(src)) {
          grunt.log.error('Source file "' + src + '" not found.');
        }

        var contents = grunt.file.read(src);

        if (contents) {
          contents = contents.replace(new RegExp('<script type="text/javascript" grunt-outline="(.*?)">([\\s\\S]*?)</script>', 'g'), function(match, dest_key, $1) {
            // Process any curly tags in content

            var dest = options[dest_key];

            grunt.file.write(dest.path + '/' + dest.file, $1);

            return '<script type="text/javascript" src="' + dest.file + '"></script>';
          });

          grunt.file.write(src, contents);
        }

        grunt.log.ok('File "' + src + '" updated.');

        });
    });

    if (this.errorCount) { return false; }
  });
};